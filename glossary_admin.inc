<?php

/**
 * @param $form
 * @param $form_state
 * @return mixed
 * Add Term Form
 */
function glossary_term_add($form, &$form_state)
{

    $form['description'] = array(
        '#type' => 'item',
        '#title' => t('Add glossary terms'),
    );
    // This is the first form element. It's a textfield with a label, "Name"
    $form['term_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter glossary term name'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => true,
        '#weight' => 10,
    );

    $form['term_description'] = array(
        '#type' => 'text_format',
        '#title' => t('Description of the glossary term'),
        '#format' => 'full_html',
        '#description' => t('Enter the description of the glossary term as you would like it to appear when viewing the term'),
        '#required' => true,
        '#weight' => 20,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Add glossary term'),
        '#weight' => 30,
    );

    return $form;
}


/**
 * Glossary configuration validate
 */
function glossary_term_add_validate($form, &$form_state)
{
    //Check if term already exists
    if (taxonomy_get_term_by_name($form_state['values']['term_name'], 'glossary')) {
        form_set_error('term_name', t('The term entered already exists'));
    }
}


/**
 * Glossary configuration submit
 */
function glossary_term_add_submit($form, &$form_state)
{

    //Load the glossary vocabulary
    $vocabulary = taxonomy_vocabulary_machine_name_load('glossary');

    //Get the new term values from the form state
    $term = $form_state['values']['term_name'];
    $term_description = $form_state['values']['term_description']['value'];
    $format = $form_state['values']['term_description']['format'];

    $new_term = new stdClass();
    $new_term->vid = $vocabulary->vid; // The ID of the parent vocabulary
    $new_term->name = $term; // The name of the term
    $new_term->format = $format;
    $new_term->description = $term_description;
    $new_term->parent = 0; // This tells taxonomy that this is a top-level term

    taxonomy_term_save($new_term);
}

/**
 * @param $form
 * @param $form_state
 * @return mixed
 * Provides table to list all terms with links to edit and delete
 */
function glossary_list_terms($form, &$form_state)
{

    //If its the first load of the form set the storage step state to step1
    if (empty($form_state['storage']['step'])) {
        $form_state['storage']['step'] = 1;
    }

    if ($form_state ['storage']['step'] == 1) {

        $form['description'] = array(
            '#type' => 'item',
            '#title' => t('List glossary terms'),
        );

        //Load vocabulary
        $vocabulary = taxonomy_vocabulary_machine_name_load('glossary');
        $terms = taxonomy_get_tree($vocabulary->vid, 0, 1);

        $header = array();
        $rows = array();

        //Build the header
        $header = array(
            'name' => t('Term name'),
            'description' => t('Description'),
            'link' => t('Edit'),
        );

        //Split the terms into rows
        foreach ($terms as $key => $term) {
            $rows[] = array(
                'tid' => $term->tid,
                'name' => l($term->name, 'taxonomy/term/' . $term->tid),
                'description' => $term->description,
                'link' => l('edit', 'admin/glossary/' . $term->tid . '/edit'),
            );
        }

        //Create options keyed by tids
        $options = array();
        foreach ($rows as $row) {
            $options[$row['tid']] = array(
                'name' => $row['name'],
                'description' => $row['description'],
                'link' => $row['link'],
            );
        }

        //Create the table
        $form['table'] = array
        (
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $options,
            '#empty' => t('No users found'),
        );

        //Add a delete button
        $form['submit'] = array
        (
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#name' => 'delete',
            '#submit' => array('glossary_list_terms_submit'),
        );
    }

    if ($form_state ['storage']['step'] == 2) {
        $form['description'] = array(
            '#type' => 'item',
            '#title' => t('Confirm Step'),
        );

        $values = $form_state['values']['table'];

        $terms = array();

        foreach ($values as $key => $value) {
            //Collect the terms that have been selected
            if ($value > 0) {
                $terms[] = $key;
            }
        }

        $form_state['storage']['values'] = $terms;



        return confirm_form(
            $form,
            t('Are you sure you want to delete this item?'),
            'admin/glossary/list',
            t('This action cannot be undone.'),
            t('Delete'),
            t('Cancel')
        );
    }
    return $form;
}

/**
 * @param $form
 * @param $form_state
 * Submit function for glossary_list_terms
 */
function glossary_list_terms_submit($form, &$form_state)
{
    //Set the step to 2 or process the items for deletion
    if ($form_state['storage']['step'] == 1 && $form_state['triggering_element']['#name'] == 'delete') {
        $form_state['storage']['step'] = 2;
    }
    else {
        //Delete the terms
        if (!empty($form_state['storage']['values'])) {
            $items = $form_state['storage']['values'];
            foreach($items as $item) {
                taxonomy_term_delete($item);
            }
        }
        //Reset the step to one
        $form_state['storage']['step'] = 1;
    }
    $form_state['rebuild'] = TRUE;
    return;
}

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function glossary_edit_terms($form, &$form_state) {

    //Get the term id from the url
    $tid = arg(2);

    $form_state['storage']['term_id'] = $tid;

    $form['description'] = array(
        '#type' => 'item',
        '#title' => 'Edit Glossary Term - ' . $tid,
    );

    $term = taxonomy_term_load($tid);

    $form['term_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Modify the glossary term name'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => true,
        '#default_value' => $term->name,
        '#weight' => 10
    );

    $form['description'] = array(
        '#type' => 'text_format',
        '#title' => t('Modify the glossary term description'),
        '#default_value' => $term->description,
        '#format' => 'full_html',
        '#weight' => 20,
    );

    //Add a save button
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#name' => 'save',
        '#submit' => array('glossary_edit_terms_submit'),
        '#weight' => 30,
    );

    return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function glossary_edit_terms_submit(&$form, &$form_state) {
    //Load the term id from the form storage
    $tid = $form_state['storage']['term_id'];

    //Load the term
    $term = taxonomy_term_load($tid);

    //Update the term from the form_state
    $term->name = $form_state['values']['term_name'];
    $term->description = $form_state['values']['description']['value'];
    $term->format = $form_state['values']['description']['format'];

    taxonomy_term_save($term);

    //@todo : Validate fields

}