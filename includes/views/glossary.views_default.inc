<?php

/**
 * Implements hook_views_default_views().
 */
function glossary_views_default_views() {
    // exported view goes here
    $view = new view();
    $view->name = 'glossary_terms';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'taxonomy_term_data';
    $view->human_name = 'Glossary terms';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Glossary';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['row_plugin'] = 'fields';
    /* Field: Taxonomy term: Term description */
    $handler->display->display_options['fields']['description']['id'] = 'description';
    $handler->display->display_options['fields']['description']['table'] = 'taxonomy_term_data';
    $handler->display->display_options['fields']['description']['field'] = 'description';
    $handler->display->display_options['fields']['description']['label'] = '';
    $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
    /* Contextual filter: Taxonomy term: Term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['title_enable'] = TRUE;
    $handler->display->display_options['arguments']['tid']['title'] = '%1';
    $handler->display->display_options['arguments']['tid']['breadcrumb'] = '%1';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
        'glossary' => 'glossary',
    );
    /* Filter criterion: Taxonomy vocabulary: Machine name */
    $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
    $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
    $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
    $handler->display->display_options['filters']['machine_name']['value'] = array(
        'glossary' => 'glossary',
    );

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'glossary/%';

    // return views
    $views[$view->name] = $view;
    return $views;
}