(function ($) {
    Drupal.behaviors.toggleGlossary = {
        attach:function()
        {
            $("span[class^='glossary-popup-container-']").mouseover(function() {
                $(this).find("span.glossary-info-box").show();
            })
                .mouseout(function() {
                    $(this).find("span.glossary-info-box").hide();
                })
        }
    };
})(jQuery);
